---
published: true
title: Ma Dada recrute un.e délégué.e général.e (indépendant.e - 3 jours/semaine)
layout: post
author: L'équipe Ma Dada
category: articles
tags:
- madada
- actualité
- recrutement

---

### L'association

Ma Dada est une plateforme qui vise à faciliter l’accès à des informations détenues par les autorités publiques. Lancé en octobre 2019 par l'association Open Knowledge France, le site [Ma Dada](https://madada.fr) a pour objectif de faciliter l’exercice du droit d’accès à l’information publique en France. Ma Dada permet d’interroger plus de 50 000 autorités publiques françaises. Toutes les demandes, les réponses, et les documents communiqués sont directement publiés sur le site.
Notre site revendique déjà plus de 1900 demandes de la part de 430 citoyens et associations.

Le site Ma Dada est actuellement géré par une équipe de 5 personnes qui contribuent essentiellement de manière bénévole. Des utilisateurs, juristes, et journalistes interviennent de manière plus ponctuelle et nous aident à améliorer le service.

### Le poste

Suite à la [participation à l'Accélérateur d'Initiatives Citoyennes](https://citoyens.transformation.gouv.fr/laureats.html) porté par le Ministère de la transformation et de la fonction publiques, l'association Open Knowledge France a obtenu une subvention pour renforcer son développement et développer de nouvelles collaborations avec l'administration.

Sur les deux années à venir, notre association se fixe notamment les objectifs suivants :

- Améliorer le taux de satisfaction des demandes en tissant de meilleures relations avec l'administration : passer d'actuellement 20% des demandes recevant in fine le document demandé à 40% (l'objectif étant évidemment d'aller au-delà sur le long terme).
- Étendre la communauté à 2000 demandeurs et permettre à un plus grand nombre de connaître Ma Dada et le droit d'accès.
- Accroître l'impact de Ma Dada en accompagnant chaque année une trentaine d'associations et collectifs citoyens dans leurs demandes.

Grâce à cette subvention, Ma Dada recrute un(e) délégué(e) général(e), qui sera son premier salarié(e). Cette personne aura notamment pour mission, sous l'autorité du conseil d'administration et en concertation avec les membres de l'association :

- de préciser et mettre en œuvre la stratégie de développement de Ma Dada ;
- d'accompagner les demandeurs, en lien avec les bénévoles de l'association ;
- de mettre en œuvre une stratégie de plaidoyer en lien avec les associations pertinentes (Ouvre Boite, Transparency France, association des journalistes pour la transparence…) ;
- d'assurer la gestion de l'association (budget, recherche de financements) et ses démarches administratives et financières avec l'appui de notre comptable ;
- de développer les partenariats avec les acteurs de l'administration publique et de représenter, avec les membres du conseil d'administration, l'association dans ses différentes activités ;
- de réaliser les actions de communication de l'association ;
- de recruter et d'animer un réseau de bénévoles pour la gestion courante du site (support aux usagers, appui aux administrations publiques...) ;
- de recruter et d'encadrer les stagiaires et services civiques de l'association ;
- de faire le lien avec l'équipe technique pour assurer le bon fonctionnement et les développements techniques du site ;
- de participer au réseau des sites sœurs de Ma Dada à l'international pour échanger des bonnes pratiques.

L'association ne dispose pas, pour l'heure, de locaux. Le travail est intégralement effectué en télétravail, notre équipe étant disséminée en France.

Compte tenu des capacités financières actuelles de l'association, le recrutement est effectué en indépendant (auto-entreprenariat, portage salarial, coopérative d'activité...) pour une durée d'un an, renouvelable. L'association envisage de pérenniser ce poste, si elle obtient les financements nécessaires.

### Profil recherché

Diplôme de niveau bac+5, expérience professionnelle minimale de deux ans souhaitée, une expérience de gestion et d'animation d'une structure associative est un plus.

- Polyvalence, autonomie, capacité d'animation en s'appuyant sur les compétences des membres et bénévoles.
- Capacité à travailler à distance en ligne et à animer un collectif en ligne.
- Bonne maîtrise des outils informatiques courants (bureautique, web, mail...).
- L'adhésion aux valeurs de l'association est essentielle. Une expérience associative est la bienvenue.
- Une connaissance des politiques et acteurs du monde de l'information publique serait un atout, de même qu'une bonne connaissance des institutions publiques.

Rémunération : 250-300€ par jour, selon expérience, pour 3 jours par semaine durant un an. Renouvelable selon les financements obtenus et possibilité d'évolution à plein temps selon l'évolution des financements obtenus et la charge de travail.

Prise de poste souhaitée : 1° Janvier 2023 (négociable).

Candidatures (cv et lettre de motivation à adresser au plus tard le 1° Décembre 2022) :
contact@madada.fr
